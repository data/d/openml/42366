# OpenML dataset: residential_building

https://www.openml.org/d/42366

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Mohammad H. Rafiei  
**Source**: UCI - [original](http://archive.ics.uci.edu/ml/datasets/Residential+Building+Data+Set) - Date unknown  
**Please cite**:   

**Residential Building Dataset**

Dataset includes construction cost, sale prices, project variables, and economic variables corresponding to real estate single-family residential apartments in Tehran, Iran.

**Attribute Information**

Totally 105: 8 project physical and financial variables, 19 economic variables and indices in 5 time lag numbers (5*19 = 95), and two output variables that are construction costs and sale prices (Output-V.9, Ouput-V.10).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42366) of an [OpenML dataset](https://www.openml.org/d/42366). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42366/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42366/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42366/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

